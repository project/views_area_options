<?php
// $Id $

/**
 * @file
 *
 */

/**
 * Implements hook_views_data_alter().
 */
function views_area_options_views_data_alter(&$data) {
  $data['views']['block'] = array(
    'title' => t('Block'),
    'help' => t('Embed a block in an Area, such as Header, Footer, Empty Text.'),
    'area' => array(
      'handler' => 'views_area_options_block_handler_area_view',
    ),
  );
}

/**
 * Implements hook_views_handlers().
 */
function views_area_options_views_handlers() {
  return array(
    'handlers' => array(
      'views_area_options_block_handler_area_view' => array(
        'parent' => 'views_handler_area',
      ),
    ),
  );
}